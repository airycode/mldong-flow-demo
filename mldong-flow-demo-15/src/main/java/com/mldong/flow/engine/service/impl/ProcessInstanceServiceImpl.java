package com.mldong.flow.engine.service.impl;

import cn.hutool.core.lang.Dict;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mldong.flow.engine.entity.ProcessDefine;
import com.mldong.flow.engine.entity.ProcessInstance;
import com.mldong.flow.engine.entity.ProcessTask;
import com.mldong.flow.engine.enums.FlowConst;
import com.mldong.flow.engine.enums.ProcessInstanceStateEnum;
import com.mldong.flow.engine.enums.TaskStateEnum;
import com.mldong.flow.engine.mapper.ProcessInstanceMapper;
import com.mldong.flow.engine.mapper.ProcessTaskMapper;
import com.mldong.flow.engine.service.ProcessInstanceService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 *
 * 流程实例服务实现类
 * @author mldong
 * @date 2023/5/30
 */
@Service
@AllArgsConstructor
public class ProcessInstanceServiceImpl implements ProcessInstanceService {
    private final ProcessInstanceMapper processInstanceMapper;
    private final ProcessTaskMapper processTaskMapper;
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void finishProcessInstance(Long processInstanceId) {
        ProcessInstance processInstance = new ProcessInstance();
        processInstance.setId(processInstanceId);
        processInstance.setState(ProcessInstanceStateEnum.FINISHED.getCode());
        processInstanceMapper.updateById(processInstance);
    }

    @Override
    public ProcessInstance createProcessInstance(ProcessDefine processDefine, String operator, Dict args) {
        ProcessInstance processInstance = new ProcessInstance();
        processInstance.setProcessDefineId(processDefine.getId());
        processInstance.setOperator(operator);
        processInstance.setState(ProcessInstanceStateEnum.DOING.getCode());
        // 业务流水号从流程变量中获取
        processInstance.setBusinessNo(args.getStr(FlowConst.BUSINESS_NO));
        processInstance.setVariable(JSONUtil.toJsonStr(args));
        saveProcessInstance(processInstance);
        return processInstance;
    }

    @Override
    public void addVariable(Long processDefineId, Dict args) {
        ProcessInstance processInstance = processInstanceMapper.selectById(processDefineId);
        Dict newDict = Dict.create();
        newDict.putAll(JSONUtil.toBean(processInstance.getVariable(),Dict.class));
        newDict.putAll(args);
        ProcessInstance up = new ProcessInstance();
        up.setId(processDefineId);
        up.setVariable(JSONUtil.toJsonStr(newDict));
        processInstanceMapper.updateById(up);
    }

    @Override
    public void removeVariable(Long processDefineId, String... keys) {
        ProcessInstance processInstance = processInstanceMapper.selectById(processDefineId);
        Dict oldDict = JSONUtil.toBean(processInstance.getVariable(),Dict.class);
        for (int i = 0; i < keys.length; i++) {
            oldDict.remove(keys[i]);
        }
        ProcessInstance up = new ProcessInstance();
        up.setId(processDefineId);
        up.setVariable(JSONUtil.toJsonStr(oldDict));
        processInstanceMapper.updateById(up);
    }

    @Override
    public void saveProcessInstance(ProcessInstance processInstance) {
        Date now = new Date();
        processInstance.setCreateTime(now);
        processInstance.setUpdateTime(now);
        processInstanceMapper.insert(processInstance);
    }


    @Override
    public void interrupt(Long processInstanceId, String operator) {
        Date now = new Date();
        // 1. 将该流程实例产生的任务状态修改为终止
        ProcessTask processTask = new ProcessTask();
        processTask.setTaskState(TaskStateEnum.INTERRUPT.getCode());
        processTask.setUpdateTime(now);
        processTask.setUpdateUser(operator);
        processTaskMapper.update(processTask,
                Wrappers.lambdaUpdate(ProcessTask.class)
                        .eq(ProcessTask::getProcessInstanceId,processInstanceId)
                        .eq(ProcessTask::getTaskState,TaskStateEnum.DOING.getCode())
        );
        // 2. 将该流程实例状态修改为终止
        ProcessInstance processInstance = new ProcessInstance();
        processInstance.setState(ProcessInstanceStateEnum.INTERRUPT.getCode());
        processInstance.setUpdateTime(now);
        processInstance.setUpdateUser(operator);
        processInstanceMapper.update(processInstance,
                Wrappers.lambdaUpdate(ProcessInstance.class)
                        .eq(ProcessInstance::getId,processInstanceId)
                        .eq(ProcessInstance::getState,ProcessInstanceStateEnum.DOING.getCode())
        );
    }

    @Override
    public void resume(Long processInstanceId, String operator) {
        Date now = new Date();
        ProcessInstance processInstance = processInstanceMapper.selectById(processInstanceId);
        // 1. 更新流程实例状态为进行中
        processInstance.setUpdateTime(now);
        processInstance.setUpdateUser(operator);
        processInstance.setState(ProcessInstanceStateEnum.DOING.getCode());
        processInstanceMapper.updateById(processInstance);
        // 2.被终止的任务状态修改为进行中
        ProcessTask processTask = new ProcessTask();
        processTask.setUpdateUser(operator);
        processTask.setUpdateTime(now);
        processTask.setTaskState(TaskStateEnum.DOING.getCode());
        processTaskMapper.update(processTask,
                Wrappers.lambdaQuery(ProcessTask.class)
                        .eq(ProcessTask::getProcessInstanceId,processInstance)
                        .eq(ProcessTask::getTaskState,TaskStateEnum.INTERRUPT.getCode()));
    }

    @Override
    public void pending(Long processInstanceId, String operator) {
        Date now = new Date();
        // 1. 将该流程实例产生的任务状态修改为挂起
        ProcessTask processTask = new ProcessTask();
        processTask.setTaskState(TaskStateEnum.PENDING.getCode());
        processTask.setUpdateTime(now);
        processTask.setUpdateUser(operator);
        processTaskMapper.update(processTask,
                Wrappers.lambdaUpdate(ProcessTask.class)
                        .eq(ProcessTask::getProcessInstanceId,processInstanceId)
                        .eq(ProcessTask::getTaskState,TaskStateEnum.DOING.getCode())
        );
        // 2. 将该流程实例状态修改为挂起
        ProcessInstance processInstance = new ProcessInstance();
        processInstance.setState(ProcessInstanceStateEnum.PENDING.getCode());
        processInstance.setUpdateTime(now);
        processInstance.setUpdateUser(operator);
        processInstanceMapper.update(processInstance,
                Wrappers.lambdaUpdate(ProcessInstance.class)
                        .eq(ProcessInstance::getId,processInstanceId)
                        .eq(ProcessInstance::getState,ProcessInstanceStateEnum.DOING.getCode())
        );
    }

    @Override
    public void activate(Long processInstanceId, String operator) {
        Date now = new Date();
        ProcessInstance processInstance = processInstanceMapper.selectById(processInstanceId);
        // 1. 更新流程实例状态为进行中
        processInstance.setUpdateTime(now);
        processInstance.setUpdateUser(operator);
        processInstance.setState(ProcessInstanceStateEnum.DOING.getCode());
        processInstanceMapper.updateById(processInstance);
        // 2.被终止的任务状态修改为进行中
        ProcessTask processTask = new ProcessTask();
        processTask.setUpdateUser(operator);
        processTask.setUpdateTime(now);
        processTask.setTaskState(TaskStateEnum.DOING.getCode());
        processTaskMapper.update(processTask,
                Wrappers.lambdaQuery(ProcessTask.class)
                        .eq(ProcessTask::getProcessInstanceId,processInstance)
                        .eq(ProcessTask::getTaskState,TaskStateEnum.INTERRUPT.getCode()));
    }


    @Override
    public void updateProcessInstance(ProcessInstance processInstance) {
        processInstance.setUpdateTime(new Date());
        processInstanceMapper.updateById(processInstance);
    }
}
