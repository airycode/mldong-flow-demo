package com.mldong.flow.engine.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.entity.ProcessTask;
import com.mldong.flow.engine.enums.TaskStateEnum;
import com.mldong.flow.engine.mapper.ProcessTaskMapper;
import com.mldong.flow.engine.model.TaskModel;
import com.mldong.flow.engine.service.ProcessTaskService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * 流程任务服务实现类
 * @author mldong
 * @date 2023/5/30
 */
@Service
@AllArgsConstructor
public class ProcessTaskServiceImpl implements ProcessTaskService {
    private final ProcessTaskMapper processTaskMapper;
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveProcessTask(ProcessTask processTask) {
        processTaskMapper.insert(processTask);
    }

    @Override
    public void updateProcessTask(ProcessTask processTask) {
        processTaskMapper.updateById(processTask);
    }

    @Override
    public List<ProcessTask> getDoingTaskList(Long processInstanceId) {
        List<ProcessTask> processTaskList = processTaskMapper.selectList(
                Wrappers.lambdaQuery(ProcessTask.class)
                        .eq(ProcessTask::getProcessInstanceId,processInstanceId)
                        .eq(ProcessTask::getTaskState, TaskStateEnum.DOING.getCode())
        );
        return processTaskList;
    }

    @Override
    public void finishProcessTask(Long processTaskId,String operator) {
        ProcessTask processTask = new ProcessTask();
        processTask.setId(processTaskId);
        processTask.setTaskState(TaskStateEnum.FINISHED.getCode());
        processTask.setUpdateTime(new Date());
        processTask.setUpdateUser(operator);
        processTask.setOperator(operator);
        processTaskMapper.updateById(processTask);
    }

    @Override
    public List<ProcessTask> createTask(TaskModel taskModel, Execution execution) {
        List<ProcessTask> processTaskList = new ArrayList<>();
        Date now = new Date();
        ProcessTask processTask = new ProcessTask();
        processTask.setTaskName(taskModel.getName());
        processTask.setDisplayName(taskModel.getDisplayName());
        processTask.setTaskState(TaskStateEnum.DOING.getCode());
        processTask.setProcessInstanceId(execution.getProcessInstanceId());
        processTask.setVariable(JSONUtil.toJsonStr(execution.getArgs()));
        processTask.setCreateTime(now);
        processTask.setUpdateTime(now);
        processTaskMapper.insert(processTask);
        execution.setProcessTask(processTask);
        processTaskList.add(processTask);
        return processTaskList;
    }

    @Override
    public ProcessTask getById(Long id) {
        return processTaskMapper.selectById(id);
    }
}
