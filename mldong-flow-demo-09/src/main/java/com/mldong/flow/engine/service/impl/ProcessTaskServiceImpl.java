package com.mldong.flow.engine.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mldong.flow.engine.entity.ProcessTask;
import com.mldong.flow.engine.enums.TaskStateEnum;
import com.mldong.flow.engine.mapper.ProcessTaskMapper;
import com.mldong.flow.engine.service.ProcessTaskService;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

/**
 *
 * 流程任务服务实现类
 * @author mldong
 * @date 2023/5/30
 */
public class ProcessTaskServiceImpl implements ProcessTaskService {
    private SqlSessionFactory sqlSessionFactory;
    public ProcessTaskServiceImpl(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }
    @Override
    public void saveProcessTask(ProcessTask processTask) {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        ProcessTaskMapper processTaskMapper = sqlSession.getMapper(ProcessTaskMapper.class);
        processTaskMapper.insert(processTask);
        sqlSession.close();
    }

    @Override
    public List<ProcessTask> getDoingTaskList(Long processInstanceId) {
        SqlSession sqlSession = sqlSessionFactory.openSession(false);
        ProcessTaskMapper processTaskMapper = sqlSession.getMapper(ProcessTaskMapper.class);
        List<ProcessTask> processTaskList = processTaskMapper.selectList(
                Wrappers.lambdaQuery(ProcessTask.class)
                        .eq(ProcessTask::getProcessInstanceId,processInstanceId)
                        .eq(ProcessTask::getTaskState, TaskStateEnum.DOING.getCode())
        );
        sqlSession.close();
        return processTaskList;
    }
}
