package com.mldong.flow.engine.enums;
/**
 *
 * 错误枚举
 * @author mldong
 * @date 2023/5/1
 */
public enum ErrEnum {
    NOT_FOUND_NEXT_NODE(1001,"decision节点无法确定下一步执行路线")
    ;
    private final Integer code;

    private final String message;


    ErrEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
