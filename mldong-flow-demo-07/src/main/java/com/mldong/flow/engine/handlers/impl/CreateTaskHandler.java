package com.mldong.flow.engine.handlers.impl;


import cn.hutool.core.util.RandomUtil;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.entity.ProcessTask;
import com.mldong.flow.engine.enums.TaskStateEnum;
import com.mldong.flow.engine.handlers.IHandler;
import com.mldong.flow.engine.model.TaskModel;

/**
 * 创建任务处理器
 * @author mldong
 * @date 2023/5/16
 */
public class CreateTaskHandler implements IHandler {
    private TaskModel taskModel;
    public CreateTaskHandler(TaskModel taskModel) {
        this.taskModel = taskModel;
    }
    @Override
    public void handle(Execution execution) {
        ProcessTask processTask = new ProcessTask();
        processTask.setId(RandomUtil.randomLong());
        processTask.setTaskName(this.taskModel.getName());
        processTask.setDisplayName(this.taskModel.getDisplayName());
        processTask.setTaskState(TaskStateEnum.DOING.getCode());
        execution.setProcessTask(processTask);
        System.out.println("创建任务："+processTask.getTaskName()+","+processTask.getDisplayName());
        execution.addTask(processTask);
    }
}
