package com.mldong.flow.engine.enums;

import java.util.Arrays;

/**
 *
 * 任务状态枚举
 * @author mldong
 * @date 2023/5/17
 */
public enum TaskStateEnum {
    DOING(10,"进行中"),
    FINISHED(20, "已完成"),
    ;
    private final Integer code;

    private final String message;


    TaskStateEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
    /**
     * code转成枚举
     * @param code
     * @return
     */
    public static TaskStateEnum codeOf(Integer code) {
        return Arrays.stream(TaskStateEnum.class.getEnumConstants()).filter(e -> e.getCode().equals(code)).findAny().orElse(DOING);
    }
}
