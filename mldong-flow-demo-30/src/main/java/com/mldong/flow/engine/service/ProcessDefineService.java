package com.mldong.flow.engine.service;

import com.mldong.flow.engine.entity.ProcessDefine;
import com.mldong.flow.engine.model.ProcessModel;

import java.io.InputStream;

/**
 *
 * 流程定义服务
 * @author mldong
 * @date 2023/5/30
 */
public interface ProcessDefineService {
    /**
     * 部署流程定义文件，同name存在多个版本
     * @param inputStream
     * @see #deploy(byte[])
     * @return Long
     * @see Long deploy(byte[] bytes);
     */
    Long deploy(InputStream inputStream);
    /**
     * 部署流程定义文件，同name存在多个版本
     * @param bytes
     * @return Long
     */
    Long deploy(byte[] bytes);
    /**
     * 部署流程定义文件，同name存在多个版本
     * @param defineJsonStr
     * @see #deploy(byte[])
     * @return Long
     * @see Long deploy(byte[] bytes);
     */
    Long deploy(String defineJsonStr);
    /**
     * 重新部署定义文件，按id更新json
     * @param processDefineId
     * @param inputStream
     * @return Long
     */
    void redeploy(Long processDefineId,InputStream inputStream);
    /**
     * 重新部署定义文件，按id更新json
     * @param processDefineId
     * @param bytes
     * @return Long
     */
    void redeploy(Long processDefineId,byte[] bytes);
    /**
     * 重新部署定义文件，按id更新json
     * @param defineJsonStr
     * @return Long
     */
    void redeploy(Long processDefineId,String defineJsonStr);

    /**
     * 卸载流程
     * @param processDefineId
     */
    public void unDeploy(Long processDefineId);

    /**
     * 只更新type
     * @param processDefineId
     * @param type
     */
    void updateType(Long processDefineId, String type);

    /**
     * 根据ID获取流程定义对象
     * @param processDefineId
     * @return
     */
    ProcessDefine getById(Long processDefineId);

    /**
     * 根据id获取流程模型
     * @param processDefineId
     * @return
     */
    ProcessModel getProcessModel(Long processDefineId);

    /**
     * 流程定义转成流程模型
     * @param processDefine
     * @return
     */
    ProcessModel processDefineToModel(ProcessDefine processDefine);

    /**
     * 根据id获取定义json字符串
     * @param processDefineId
     * @return
     */
    String getDefineJsonStr(Long processDefineId);

    /**
     * 通过流程定义名称和版本获取流程定义
     * @param name
     * @param version
     * @return
     */
    ProcessDefine getProcessDefineByVersion(String name,Integer version);
}
