package com.mldong.flow.engine.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 流程委托代理
 * </p>
 *
 * @author mldong
 * @since 2023-12-06
 */
@Getter
@Setter
@TableName("wf_process_surrogate")
public class ProcessSurrogate implements Serializable  {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    private String processName;

    private String operator;

    private String surrogate;

    private Date startTime;

    private Date endTime;

    private Integer enabled;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateUser;

}
