package com.mldong.flow.engine.handlers.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.entity.ProcessDefine;
import com.mldong.flow.engine.entity.ProcessInstance;
import com.mldong.flow.engine.enums.ProcessEventTypeEnum;
import com.mldong.flow.engine.event.ProcessEvent;
import com.mldong.flow.engine.event.ProcessPublisher;
import com.mldong.flow.engine.handlers.IHandler;
import com.mldong.flow.engine.model.EndModel;
import com.mldong.flow.engine.model.ProcessModel;
import com.mldong.flow.engine.model.SubProcessModel;

/**
 *
 * 结束流程实例的处理器
 * @author mldong
 * @date 2023/5/29
 */
public class EndProcessHandler implements IHandler {
    private EndModel endModel;
    public EndProcessHandler(EndModel endModel) {
        this.endModel = endModel;
    }
    @Override
    public void handle(Execution execution) {
        execution.getEngine().processInstanceService().finishProcessInstance(execution.getProcessInstanceId());
        // 发布流程实例结束事件
        ProcessPublisher.notify(ProcessEvent.builder().eventType(ProcessEventTypeEnum.PROCESS_INSTANCE_END).sourceId(execution.getProcessInstanceId()).build());
        ProcessInstance processInstance = execution.getProcessInstance();
        if(ObjectUtil.isNotNull(processInstance.getParentId())) {
            // 如果子流程存在父流程实例，则执行父流程的子流程节点模型方法
            ProcessInstance parentInstance = execution.getEngine().processInstanceService().getById(processInstance.getParentId());
            if(parentInstance == null) return;
            ProcessModel pm = execution.getEngine().processDefineService().getProcessModel(parentInstance.getProcessDefineId());
            if(pm == null) return;
            SubProcessModel spm = (SubProcessModel)pm.getNode(processInstance.getParentNodeName());
            Execution newExecution = new Execution();
            newExecution.setEngine(execution.getEngine());
            newExecution.setProcessModel(pm);
            newExecution.setProcessInstance(parentInstance);
            newExecution.setProcessInstanceId(parentInstance.getId());
            newExecution.setArgs(execution.getArgs());
            spm.execute(newExecution);
            execution.addTasks(newExecution.getProcessTaskList());
        }
    }
}