package com.mldong.flow.engine.service.impl;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mldong.flow.engine.entity.ProcessDefine;
import com.mldong.flow.engine.enums.ProcessDefineStateEnum;
import com.mldong.flow.engine.mapper.ProcessDefineMapper;
import com.mldong.flow.engine.model.ProcessModel;
import com.mldong.flow.engine.parser.ModelParser;
import com.mldong.flow.engine.service.ProcessDefineService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;

/**
 *
 * 流程定义服务实现类
 * @author mldong
 * @date 2023/5/30
 */
@Service
@AllArgsConstructor
public class ProcessDefineServiceImpl implements ProcessDefineService {
    private final ProcessDefineMapper processDefineMapper;

    @Override
    public Long deploy(InputStream inputStream) {
        return deploy(IoUtil.readBytes(inputStream));
    }

    @Override
    public Long deploy(byte[] bytes) {
        Date now = new Date();
        // 1. json定义文件转成流程模型
        ProcessModel processModel = ModelParser.parse(bytes);
        // 2. 根据名称查询，取最新版本的流程定义记录
        List<ProcessDefine> processDefineList = processDefineMapper.selectList(
                Wrappers.lambdaQuery(ProcessDefine.class)
                        .eq(ProcessDefine::getName,processModel.getName())
                        .orderByDesc(ProcessDefine::getId)
        );
        ProcessDefine processDefine = null;
        // 3.1 如果存在，则版本+1，并插入一条新的流程定义记录
        if(!processDefineList.isEmpty()) {
            processDefine = processDefineList.get(0);
            processDefine.setId(null);
            processDefine.setVersion(processDefine.getVersion()+1);
        } else {
            // 3.2 如果不存在，则版本默认为1，并插入一条新的流程定义记录
            processDefine = new ProcessDefine();
            processDefine.setVersion(0);
        }
        processDefine.setName(processModel.getName());
        processDefine.setDisplayName(processModel.getDisplayName());
        processDefine.setType(processModel.getType());
        processDefine.setCreateTime(now);
        processDefine.setUpdateTime(now);
        processDefine.setState(ProcessDefineStateEnum.ENABLE.getCode());
        processDefine.setContent(bytes);
        processDefineMapper.insert(processDefine);
        return processDefine.getId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long deploy(String json) {
        return deploy(json.getBytes());
    }

    @Override
    public void redeploy(Long processDefineId, InputStream inputStream) {
        redeploy(processDefineId,IoUtil.readBytes(inputStream));
    }

    @Override
    public void redeploy(Long processDefineId, byte[] bytes) {
        Date now = new Date();
        // 1. json定义文件转成流程模型
        ProcessModel processModel = ModelParser.parse(bytes);
        // 2. 构造模型定义信息
        ProcessDefine processDefine = new ProcessDefine();
        processDefine.setId(processDefineId);
        processDefine.setType(processModel.getType());
        processDefine.setName(processModel.getName());
        processDefine.setDisplayName(processModel.getDisplayName());
        processDefine.setUpdateTime(now);
        processDefine.setContent(bytes);
        // 3. 更新模型定义文件
        processDefineMapper.updateById(processDefine);
    }

    @Override
    public void redeploy(Long processDefineId, String json) {
        redeploy(processDefineId, json.getBytes());
    }

    @Override
    public void unDeploy(Long processDefineId) {
        ProcessDefine processDefine = new ProcessDefine();
        processDefine.setId(processDefineId);
        processDefine.setState(ProcessDefineStateEnum.DISABLE.getCode());
        processDefineMapper.updateById(processDefine);
    }

    @Override
    public void updateType(Long processDefineId, String type) {
        ProcessDefine processDefine = new ProcessDefine();
        processDefine.setId(processDefineId);
        processDefine.setType(type);
        processDefineMapper.updateById(processDefine);
    }

    @Override
    public ProcessDefine getById(Long processDefineId) {
        return processDefineMapper.selectById(processDefineId);
    }

    @Override
    public ProcessModel getProcessModel(Long processDefineId) {
        return processDefineToModel(getById(processDefineId));
    }

    @Override
    public ProcessModel processDefineToModel(ProcessDefine processDefine) {
        if(processDefine == null) return null;
        ProcessModel processModel = ModelParser.parse(processDefine.getContent());
        // 注：后续再进行缓存优化，减少频繁解析
        return processModel;
    }

    @Override
    public String getDefineJsonStr(Long id) {
        ProcessDefine processDefine = getById(id);
        if(processDefine == null) return null;
        return StrUtil.str(processDefine.getContent(), Charset.defaultCharset());
    }

    @Override
    public ProcessDefine getProcessDefineByVersion(String name, Integer version) {
        LambdaQueryWrapper<ProcessDefine> queryWrapper = Wrappers.lambdaQuery(ProcessDefine.class);
        queryWrapper.eq(ProcessDefine::getName,name);
        if(version!=null) {
            queryWrapper.eq(ProcessDefine::getVersion, version);
        }
        queryWrapper.orderByDesc(ProcessDefine::getId);
        List<ProcessDefine> processDefineList = processDefineMapper.selectList(queryWrapper);
        if(!processDefineList.isEmpty()) {
            return processDefineList.get(0);
        }
        return null;
    }

}
