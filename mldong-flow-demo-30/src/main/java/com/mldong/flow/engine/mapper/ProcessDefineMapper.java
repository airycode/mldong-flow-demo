package com.mldong.flow.engine.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mldong.flow.engine.entity.ProcessDefine;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 流程定义 Mapper 接口
 * </p>
 *
 * @author mldong
 * @since 2023-05-28
 */
@Mapper
public interface ProcessDefineMapper extends BaseMapper<ProcessDefine> {

}
