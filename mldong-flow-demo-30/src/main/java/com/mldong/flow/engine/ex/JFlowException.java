package com.mldong.flow.engine.ex;


import com.mldong.flow.engine.enums.ErrEnum;

/**
 *
 * 工作流自定义异常
 * @author mldong
 * @date 2023/5/1
 */
public class JFlowException extends RuntimeException {
    public JFlowException(ErrEnum errEnum) {}
    public JFlowException(String msg) {
        super(msg);
    }
}
