package com.mldong.flow.engine.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mldong.flow.engine.entity.ProcessCcInstance;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 流程实例抄送 Mapper 接口
 * </p>
 *
 * @author mldong
 * @since 2023-12-05
 */
@Mapper
public interface ProcessCcInstanceMapper extends BaseMapper<ProcessCcInstance> {

}
