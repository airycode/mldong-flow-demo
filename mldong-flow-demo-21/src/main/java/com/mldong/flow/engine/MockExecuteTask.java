package com.mldong.flow.engine;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.thread.ThreadUtil;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.entity.ProcessTask;

import java.util.List;

/**
 *
 * 模拟执行任务全流程
 * @author mldong
 * @date 2023/5/17
 */
public class MockExecuteTask {
    private Execution execution;
    public MockExecuteTask(Execution execution) {
        this.execution = execution;
    }
    public void run() {
        while (!execution.getEngine().processTaskService().getDoingTaskList(execution.getProcessInstanceId(), null).isEmpty()) {
            // 休眠1s
            ThreadUtil.safeSleep(1000);
            // 找到未完成的任务
            List<ProcessTask> doingTaskList = execution.getEngine().processTaskService().getDoingTaskList(execution.getProcessInstanceId(),null);
            // 取第一个未完成的任务
            ProcessTask processTask = doingTaskList.get(0);
            // 操作人从执行对象中获取
            String operator = execution.getOperator();
            // 这里只是为了测试，找到leave_17.json的任务执行人分别的1,2,3，因taskName分别为task1,task2,task3
            // 所以刚好可以使用如下转换
            // String operator = processTask.getTaskName().replace("task","");
            // 这个是超管
            //String operator = FlowConst.ADMIN_ID;
            // 这个是自动执行
            // String operator = FlowConst.ADMIN_ID;
            execution.getEngine().executeProcessTask(processTask.getId(),operator, Dict.create());
        }
    }
}
