package com.mldong.interceptor;

import com.mldong.flow.engine.FlowInterceptor;
import com.mldong.flow.engine.core.Execution;

/**
 *
 * 请假流程后置拦截器
 * @author mldong
 * @date 2023/9/10
 */
public class LeavePostInterceptor implements FlowInterceptor {
    @Override
    public void intercept(Execution execution) {
        System.out.println("请假流程后置拦截器：LeavePostInterceptor,当前节点名："+execution.getNodeModel().getName());
    }
}
