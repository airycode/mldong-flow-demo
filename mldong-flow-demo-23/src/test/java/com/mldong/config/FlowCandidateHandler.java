package com.mldong.config;

import com.mldong.flow.engine.CandidateHandler;
import com.mldong.flow.engine.entity.Candidate;
import com.mldong.flow.engine.model.TaskModel;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * 候选人自定义处理类
 * @author mldong
 * @date 2023/6/26
 */
public class FlowCandidateHandler implements CandidateHandler {
    @Override
    public List<Candidate> handle(TaskModel model) {
        List<Candidate> res = new ArrayList<>();
        res.add(Candidate.builder().userId("1").userName("admin1").build());
        res.add(Candidate.builder().userId("2").userName("admin2").build());
        res.add(Candidate.builder().userId("3").userName("admin3").build());
        // 去重验证
        res.add(Candidate.builder().userId("3").userName("admin3").build());
        return res;
    }
}
