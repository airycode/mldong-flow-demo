package com.mldong.flow.engine.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 流程实例
 * </p>
 *
 * @author mldong
 * @since 2023-05-28
 */
@Getter
@Setter
@TableName("wf_process_instance")
public class ProcessInstance implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 父流程ID，子流程实例才有值
     */
    private Long parentId;

    /**
     * 流程定义ID
     */
    private Long processDefineId;

    /**
     * 流程实例状态(10：进行中；20：已完成；30：已撤回；40：强行中止；50：挂起；99：已废弃)
     */
    private Integer state;
    /**
     * 父流程依赖的节点名称
     */
    private String parentNodeName;

    /**
     * 业务编号
     */
    private String businessNo;

    /**
     * 流程发起人
     */
    private String operator;

    /**
     * 期望完成时间
     */
    private Date expireTime;

    /**
     * 附属变量json存储
     */
    private String variable;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 创建用户
     */
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 更新用户
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateUser;


}
