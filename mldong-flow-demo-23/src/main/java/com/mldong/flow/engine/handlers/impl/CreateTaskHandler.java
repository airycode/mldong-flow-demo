package com.mldong.flow.engine.handlers.impl;


import com.mldong.flow.engine.FlowInterceptor;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.core.ServiceContext;
import com.mldong.flow.engine.entity.ProcessTask;
import com.mldong.flow.engine.handlers.IHandler;
import com.mldong.flow.engine.model.TaskModel;

import java.util.List;

/**
 * 创建任务处理器
 * @author mldong
 * @date 2023/5/16
 */
public class CreateTaskHandler implements IHandler {
    private TaskModel taskModel;
    public CreateTaskHandler(TaskModel taskModel) {
        this.taskModel = taskModel;
    }
    @Override
    public void handle(Execution execution) {
        // 创建任务
        List<ProcessTask> processTaskList = execution.getEngine().processTaskService().createTask(taskModel, execution);
        // 将任务添加到执行对象中
        execution.addTasks(processTaskList);
        // 从服务上下文中获取拦截器执行
        ServiceContext.findList(FlowInterceptor.class).forEach(flowInterceptor -> {
            flowInterceptor.intercept(execution);
        });
    }
}
