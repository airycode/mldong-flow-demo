package com.mldong.config;

import cn.hutool.core.lang.Dict;
import com.mldong.flow.engine.scheduling.IScheduler;
import org.springframework.stereotype.Component;

/**
 * @author mldong
 * @date 2023/12/5
 */
@Component
public class DefaultScheduler implements IScheduler {
    @Override
    public void addJob(String jobId, Dict args) {
        System.out.println("添加"+jobId+"到调度器");
    }

    @Override
    public void removeJob(String jobId) {
        System.out.println("将"+jobId+"从调度器移除");
    }
}
