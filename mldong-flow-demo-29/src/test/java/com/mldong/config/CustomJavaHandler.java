package com.mldong.config;

/**
 * 自定义普通java类
 * @author mldong
 * @date 2023/12/7
 */
public class CustomJavaHandler {
    public String test(String a,Integer b) {
        System.out.println("a:"+a+",b:"+b);
        return "您好,我是自定义普通java类";
    }
}
