package com.mldong.config;

import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.enums.FlowConst;
import com.mldong.flow.engine.handlers.IHandler;

/**
 * 自定义处理类
 * @author mldong
 * @date 2023/12/7
 */
public class CustomHandler implements IHandler {
    @Override
    public void handle(Execution execution) {
        System.out.println(execution.getArgs());
        execution.getArgs().put(FlowConst.CUSTOM_RETURN_VAL,"您好，我是自定义处理类");
    }
}
