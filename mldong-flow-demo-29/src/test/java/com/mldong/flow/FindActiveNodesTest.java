package com.mldong.flow;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import com.mldong.flow.engine.cfg.Configuration;
import com.mldong.flow.engine.handlers.impl.MergeBranchHandler;
import com.mldong.flow.engine.model.JoinModel;
import com.mldong.flow.engine.model.ProcessModel;
import com.mldong.flow.engine.parser.ModelParser;
import org.junit.Test;

public class FindActiveNodesTest {
    @Test
    public void findActiveNodesLeave_20() {
        new Configuration();
        // 将流程定义文件解析成流程模型
        ProcessModel processModel = ModelParser.parse(IoUtil.readBytes(this.getClass().getResourceAsStream("/leave_20.json")));
        // 找到合并节点J1
        JoinModel join1 = (JoinModel)processModel.getNode("J1");
        // 找到合并节点J1和其对应分支节点间的所有任务模型名称
        String [] activeNodes1 = new MergeBranchHandler(join1).findActiveNodes();
        // 打印合并节点J1和其对应分支节点间的所有任务模型名称
        System.err.println(StrUtil.join(",",activeNodes1));
        // 找到合并节点J2
        JoinModel join2 = (JoinModel) processModel.getNode("J2");
        // 找到合并节点J1和其对应分支节点间的所有任务模型名称
        String [] activeNodes2 = new MergeBranchHandler(join2).findActiveNodes();
        // 打印合并节点J1和其对应分支节点间的所有任务模型名称
        System.err.println(StrUtil.join(",",activeNodes2));
    }
}
