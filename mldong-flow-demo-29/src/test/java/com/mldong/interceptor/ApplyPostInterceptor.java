package com.mldong.interceptor;

import com.mldong.flow.engine.FlowInterceptor;
import com.mldong.flow.engine.core.Execution;

/**
 *
 * 请假申请节点后置拦截器
 * @author mldong
 * @date 2023/9/3
 */
public class ApplyPostInterceptor implements FlowInterceptor {
    @Override
    public void intercept(Execution execution) {
        System.out.println("请假申请节点后置拦截器：ApplyPostInterceptor");
    }
}
