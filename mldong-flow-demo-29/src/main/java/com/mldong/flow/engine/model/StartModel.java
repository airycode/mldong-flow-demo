package com.mldong.flow.engine.model;

import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.enums.ProcessEventTypeEnum;
import com.mldong.flow.engine.event.ProcessEvent;
import com.mldong.flow.engine.event.ProcessPublisher;
import lombok.Data;
/**
 *
 * 开始模型
 * @author mldong
 * @date 2023/4/25
 */
@Data
public class StartModel extends NodeModel {
    @Override
    public void exec(Execution execution) {
        // 执行开始节点自定义执行逻辑
        System.out.println(super.toString());
        // 发布流程实例开始事件
        ProcessPublisher.notify(ProcessEvent.builder().eventType(ProcessEventTypeEnum.PROCESS_INSTANCE_START).sourceId(execution.getProcessInstanceId()).build());
        runOutTransition(execution);
    }
}