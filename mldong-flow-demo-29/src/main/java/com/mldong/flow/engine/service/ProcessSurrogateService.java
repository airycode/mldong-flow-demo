package com.mldong.flow.engine.service;

/**
 * 流程代理接口
 * @author mldong
 * @date 2023/12/6
 */
public interface ProcessSurrogateService {
    /**
     * 获取代理人
     * @param operator
     * @param processName
     * @return
     */
    String getSurrogate(String operator,String processName);
}
