package com.mldong.flow.engine;

import com.mldong.flow.engine.core.Execution;
/**
 *
 * 模型行为
 * @author mldong
 * @date 2023/4/25
 */
public interface Action {
    public void execute(Execution execution);
}