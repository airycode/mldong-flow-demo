package com.mldong.flow.engine;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.thread.ThreadUtil;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.entity.ProcessTask;
import com.mldong.flow.engine.model.NodeModel;

import java.util.List;

/**
 *
 * 模拟执行任务全流程
 * @author mldong
 * @date 2023/5/17
 */
public class MockExecuteTask {
    private Execution execution;
    public MockExecuteTask(Execution execution) {
        this.execution = execution;
    }
    public void run() {
        while (!execution.getEngine().getDoingTaskList(execution.getProcessInstanceId()).isEmpty()) {
            // 休眠1s
            ThreadUtil.safeSleep(1000);
            // 找到未完成的任务
            List<ProcessTask> doingTaskList = execution.getEngine().getDoingTaskList(execution.getProcessInstanceId());
            // 取第一个未完成的任务
            ProcessTask processTask = doingTaskList.get(0);
            execution.getEngine().executeProcessTask(processTask.getId(),"", Dict.create());
            System.out.println("设置任务状态为已完成："+processTask.getTaskName()+","+processTask.getDisplayName());

        }
    }
}
