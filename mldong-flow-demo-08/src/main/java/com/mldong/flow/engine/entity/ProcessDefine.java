package com.mldong.flow.engine.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 流程定义
 * </p>
 *
 * @author mldong
 * @since 2023-05-28
 */
@Getter
@Setter
@TableName("wf_process_define")
public class ProcessDefine implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 唯一编码
     */
    private String name;

    /**
     * 显示名称
     */
    private String displayName;

    /**
     * 流程类型
     */
    private String type;

    /**
     * 流程是否可用(1可用；0不可用)
     */
    private Integer state;

    /**
     * 流程模型定义
     */
    private byte [] content;

    /**
     * 版本
     */
    private Integer version;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 更新用户
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateUser;


}
