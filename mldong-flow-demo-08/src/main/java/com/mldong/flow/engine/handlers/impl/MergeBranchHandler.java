package com.mldong.flow.engine.handlers.impl;


import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.handlers.IHandler;
import com.mldong.flow.engine.model.JoinModel;

/**
 * 合并分支操作的处理器
 * @author mldong
 * @date 2023/5/21
 */
public class MergeBranchHandler implements IHandler {
    private JoinModel joinModel;
    public MergeBranchHandler(JoinModel joinModel) {
        this.joinModel = joinModel;
    }
    @Override
    public void handle(Execution execution) {
        // 判断是否存在正在执行的任务，存在则不允许合并
        execution.setMerged(execution.getEngine().getDoingTaskList(execution.getProcessInstanceId()).isEmpty());
    }
}
