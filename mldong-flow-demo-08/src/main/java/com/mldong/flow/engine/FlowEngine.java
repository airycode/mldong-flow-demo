package com.mldong.flow.engine;

import cn.hutool.core.lang.Dict;
import com.mldong.flow.engine.cfg.Configuration;
import com.mldong.flow.engine.entity.ProcessInstance;
import com.mldong.flow.engine.entity.ProcessTask;

import java.util.List;

/**
 *
 * 流程引擎接口
 * @author mldong
 * @date 2023/5/29
 */
public interface FlowEngine {
    /**
     * 根据Configuration对象配置实现类
     * @param config 全局配置对象
     * @return FlowEngine 流程引擎
     */
    FlowEngine configure(Configuration config);
    /**
     * 部署流程
     * @param json
     */
    Long deploy(String json);
    /**
     * 根据流程定义ID、操作人ID、启动流程参数启动流程实例
     * @param id 流程定义ID
     * @param operator 操作人ID
     * @param args 启动流程参数
     * @return ProcessInstance 流程实例
     */
    ProcessInstance startProcessInstanceById(Long id, String operator, Dict args);

    /**
     * 执行流程任务
     * @param processTaskId
     * @param operator
     * @param args
     * @return
     */
    List<ProcessTask> executeProcessTask(Long processTaskId, String operator, Dict args);

    /**
     * 保存流程任务
     * @param processTask
     */
    void saveProcessTask(ProcessTask processTask);

    /**
     * 根据流程实例ID获取正在进行的任务
     * @param processInstanceId
     * @return
     */
    List<ProcessTask> getDoingTaskList(Long processInstanceId);

    /**
     * 将流程实例修改为已完成
     * @param processInstanceId
     */
    void finishProcessInstance(Long processInstanceId);
}
