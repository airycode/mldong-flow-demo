package com.mldong.flow;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.lang.Dict;
import com.mldong.flow.engine.FlowEngine;
import com.mldong.flow.engine.MockExecuteTask;
import com.mldong.flow.engine.cfg.Configuration;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.entity.ProcessInstance;
import com.mldong.flow.engine.core.FlowEngineImpl;
import org.junit.Test;
/**
 *
 * 流程引擎单元测试类
 * @author mldong
 * @date 2023/5/30
 */
public class FlowEngineTest extends BaseTest{

    @Test
    public void executeLeave_06() {
        // 1.配置引擎
        FlowEngine engine = new FlowEngineImpl().configure(new Configuration());
        // 2. 部署一条流程
        Long processDefineId = engine.deploy(IoUtil.readUtf8(this.getClass().getResourceAsStream("/leave_06.json")));
        // 3. 启动一条流程实例
        ProcessInstance processInstance = engine.startProcessInstanceById(processDefineId,"",Dict.create());
        // 4. 构建执行对象
        Execution execution = new Execution();
        execution.setEngine(engine);
        execution.setProcessInstanceId(processInstance.getId());
        execution.setProcessInstance(processInstance);
        // 5. 执行模拟执行任务方法
        new MockExecuteTask(execution).run();
    }
}
