package com.mldong.flow;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mldong.flow.engine.mapper.ProcessDefineMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

/**
 *
 * 数据库连接测试
 * @author mldong
 * @date 2023/5/28
 */
@Slf4j
public class DbTest extends BaseTest{
    @Test
    public void conn() throws Exception {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        ProcessDefineMapper processDefineMapper = sqlSession.getMapper(ProcessDefineMapper.class);
        System.err.println(processDefineMapper.selectCount(Wrappers.lambdaQuery()));
    }
}
