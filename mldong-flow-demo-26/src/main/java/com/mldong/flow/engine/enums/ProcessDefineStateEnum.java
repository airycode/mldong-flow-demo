package com.mldong.flow.engine.enums;

import java.util.Arrays;

/**
 *
 * 流程定义状态
 * @author mldong
 * @date 2023/5/28
 */
public enum ProcessDefineStateEnum {
    DISABLE(0,"不可用"),
    ENABLE(1, "可用"),
    ;
    private final Integer code;

    private final String message;


    ProcessDefineStateEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
    /**
     * code转成枚举
     * @param code
     * @return
     */
    public static ProcessDefineStateEnum codeOf(Integer code) {
        return Arrays.stream(ProcessDefineStateEnum.class.getEnumConstants()).filter(e -> e.getCode().equals(code)).findAny().orElse(ENABLE);
    }
}
