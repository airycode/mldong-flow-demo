package com.mldong.flow;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.lang.Dict;
import com.mldong.flow.engine.FlowEngine;
import com.mldong.flow.engine.MockExecuteTask;
import com.mldong.flow.engine.cfg.Configuration;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.core.FlowEngineImpl;
import com.mldong.flow.engine.entity.ProcessInstance;
import com.mldong.flow.engine.enums.FlowConst;
import com.mldong.flow.engine.impl.SpringContext;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * @author mldong
 * @date 2023/11/27
 */
public class CountersignTest extends BaseUnit{
    @Resource
    private SpringContext springContext;
    /**
     * 串行会签，全部通过
     */
    @Test
    public void executeLeave_24_1() {
        // 1.配置引擎
        FlowEngine engine = new FlowEngineImpl().configure(new Configuration(springContext));
        // 2. 部署一条流程
        Long processDefineId = engine.processDefineService().deploy(IoUtil.readUtf8(this.getClass().getResourceAsStream("/leave_24_1.json")));
        // 3. 启动一条流程实例
        Dict args = Dict.create();
        args.put("executeUserList", CollectionUtil.newArrayList("1","1","1","2","3","4"));
        ProcessInstance processInstance = engine.startProcessInstanceById(processDefineId,"1",args);
        // 4. 构建执行对象
        Execution execution = new Execution();
        execution.setEngine(engine);
        execution.setProcessInstanceId(processInstance.getId());
        execution.setProcessInstance(processInstance);
        execution.setArgs(args);
        // 5. 执行模拟执行任务方法
        new MockExecuteTask(execution).run();
    }
    /**
     * 串行会签，按数量通过
     */
    @Test
    public void executeLeave_24_2() {
        // 1.配置引擎
        FlowEngine engine = new FlowEngineImpl().configure(new Configuration(springContext));
        // 2. 部署一条流程
        Long processDefineId = engine.processDefineService().deploy(IoUtil.readUtf8(this.getClass().getResourceAsStream("/leave_24_2.json")));
        // 3. 启动一条流程实例
        Dict args = Dict.create();
        args.put("executeUserList", CollectionUtil.newArrayList("1","1","1","2","4"));
        ProcessInstance processInstance = engine.startProcessInstanceById(processDefineId,"1",args);
        // 4. 构建执行对象
        Execution execution = new Execution();
        execution.setEngine(engine);
        execution.setProcessInstanceId(processInstance.getId());
        execution.setProcessInstance(processInstance);
        execution.setArgs(args);
        // 5. 执行模拟执行任务方法
        new MockExecuteTask(execution).run();
    }

    /**
     * 串行会签，按比例通过
     */
    @Test
    public void executeLeave_24_3() {
        // 1.配置引擎
        FlowEngine engine = new FlowEngineImpl().configure(new Configuration(springContext));
        // 2. 部署一条流程
        Long processDefineId = engine.processDefineService().deploy(IoUtil.readUtf8(this.getClass().getResourceAsStream("/leave_24_3.json")));
        // 3. 启动一条流程实例
        Dict args = Dict.create();
        args.put("executeUserList", CollectionUtil.newArrayList("1","1","1","2","4"));
        ProcessInstance processInstance = engine.startProcessInstanceById(processDefineId,"1",args);
        // 4. 构建执行对象
        Execution execution = new Execution();
        execution.setEngine(engine);
        execution.setProcessInstanceId(processInstance.getId());
        execution.setProcessInstance(processInstance);
        execution.setArgs(args);
        // 5. 执行模拟执行任务方法
        new MockExecuteTask(execution).run();
    }

    /**
     * 一票否决
     */
    @Test
    public void executeLeave_24_4() {
        // 1.配置引擎
        FlowEngine engine = new FlowEngineImpl().configure(new Configuration(springContext));
        // 2. 部署一条流程
        Long processDefineId = engine.processDefineService().deploy(IoUtil.readUtf8(this.getClass().getResourceAsStream("/leave_24_4.json")));
        // 3. 启动一条流程实例
        Dict args = Dict.create();
        args.put("executeUserList", CollectionUtil.newArrayList("1","1","1","4"));
        // 增加一票否决标识
        args.put(FlowConst.COUNTERSIGN_DISAGREE_FLAG, 1);
        ProcessInstance processInstance = engine.startProcessInstanceById(processDefineId,"1",args);
        // 4. 构建执行对象
        Execution execution = new Execution();
        execution.setEngine(engine);
        execution.setProcessInstanceId(processInstance.getId());
        execution.setProcessInstance(processInstance);
        execution.setArgs(args);
        // 5. 执行模拟执行任务方法
        new MockExecuteTask(execution).run();
    }
}
