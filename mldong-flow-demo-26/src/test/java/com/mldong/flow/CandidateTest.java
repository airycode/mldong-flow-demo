package com.mldong.flow;

import cn.hutool.core.io.IoUtil;
import cn.hutool.json.JSONUtil;
import com.mldong.flow.engine.cfg.Configuration;
import com.mldong.flow.engine.model.ProcessModel;
import com.mldong.flow.engine.model.TaskModel;
import com.mldong.flow.engine.parser.ModelParser;
import org.junit.Test;

import java.util.List;
/**
 *
 * 候选人相关单元测试类
 * @author mldong
 * @date 2023/6/26
 */
public class CandidateTest {
    /**
     * 普通顺序流程，获取某个任务节点的下一个任务节点模型
     */
    @Test
    public void getNextTaskModelsTest_leave_19_1() {
        new Configuration();
        // 将流程定义文件解析成流程模型
        ProcessModel processModel = ModelParser.parse(IoUtil.readBytes(this.getClass().getResourceAsStream("/leave_19_1.json")));
        // 构造执行参数
        List<TaskModel> taskModelList = processModel.getNextTaskModels("task2");
        taskModelList.forEach(item->{
            System.err.println(item.getName()+":"+item.getDisplayName());
        });
        System.err.println(JSONUtil.toJsonStr(processModel.getNextTaskModelCandidates("task2")));
    }
    /**
     * 存在决策表达式流程，获取某个任务节点的下一个任务节点模型
     */
    @Test
    public void getNextTaskModelsTest_leave_19_2() {
        new Configuration();
        // 将流程定义文件解析成流程模型
        ProcessModel processModel = ModelParser.parse(IoUtil.readBytes(this.getClass().getResourceAsStream("/leave_19_2.json")));
        // 构造执行参数
        List<TaskModel> taskModelList = processModel.getNextTaskModels("task2");
        taskModelList.forEach(item->{
            System.err.println(item.getName()+":"+item.getDisplayName());
        });
        System.err.println(JSONUtil.toJsonStr(processModel.getNextTaskModelCandidates("task2")));
    }
    /**
     * 存在分支流程，获取某个任务节点的下一个任务节点模型
     */
    @Test
    public void getNextTaskModelsTest_leave_19_3() {
        new Configuration();
        // 将流程定义文件解析成流程模型
        ProcessModel processModel = ModelParser.parse(IoUtil.readBytes(this.getClass().getResourceAsStream("/leave_19_3.json")));
        // 构造执行参数
        List<TaskModel> taskModelList = processModel.getNextTaskModels("task2");
        taskModelList.forEach(item->{
            System.err.println(item.getName()+":"+item.getDisplayName());
        });
        System.err.println(JSONUtil.toJsonStr(processModel.getNextTaskModelCandidates("task2")));
    }
}
