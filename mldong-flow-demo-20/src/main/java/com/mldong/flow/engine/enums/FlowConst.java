package com.mldong.flow.engine.enums;
/**
 *
 * 流程常量
 * @author mldong
 * @date 2023/6/9
 */
public interface FlowConst {
    // 业务流程号
    String BUSINESS_NO = "BUSINESS_NO";
    // 超级管理员ID
    String ADMIN_ID = "flow.admin";
    // 自动执行ID
    String AUTO_ID = "flow.auto";
}
