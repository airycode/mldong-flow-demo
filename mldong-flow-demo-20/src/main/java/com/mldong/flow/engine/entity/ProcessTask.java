package com.mldong.flow.engine.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 流程任务
 * </p>
 *
 * @author mldong
 * @since 2023-05-28
 */
@Getter
@Setter
@TableName("wf_process_task")
public class ProcessTask implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 流程实例ID
     */
    private Long processInstanceId;

    /**
     * 任务名称编码
     */
    private String taskName;

    /**
     * 任务显示名称
     */
    private String displayName;

    /**
     * 任务类型(0：主办任务；1：协办任务)
     */
    private Integer taskType;

    /**
     * 参与类型(0：普通参与；1：会签参与)
     */
    private Integer performType;

    /**
     * 任务状态(10：进行中；20：已完成；30：已撤回；40：强行中止；50：挂起；99：已废弃)
     */
    private Integer taskState;

    /**
     * 任务处理人
     */
    private String operator;

    /**
     * 任务完成时间
     */
    private Date finishTime;

    /**
     * 任务期待完成时间
     */
    private Date expireTime;
    /**
     * 任务处理表单KEY
     */
    private String formKey;
    /**
     * 父任务ID
     */
    private Long taskParentId;
    /**
     * 附属变量json存储
     */
    private String variable;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 创建用户
     */
    @TableField(fill = FieldFill.INSERT)
    private String createUser;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 更新用户
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateUser;


}
