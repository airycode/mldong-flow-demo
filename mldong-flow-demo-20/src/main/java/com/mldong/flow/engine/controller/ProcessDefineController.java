package com.mldong.flow.engine.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProcessDefineController {
    @GetMapping("/")
    public String index() {
        return "hello world";
    }
}
