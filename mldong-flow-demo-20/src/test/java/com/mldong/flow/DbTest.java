package com.mldong.flow;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mldong.flow.engine.mapper.ProcessDefineMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import javax.annotation.Resource;

/**
 *
 * 数据库连接测试
 * @author mldong
 * @date 2023/5/28
 */
@Slf4j
public class DbTest extends BaseUnit{
    @Resource
    private ProcessDefineMapper processDefineMapper;
    @Test
    public void conn() throws Exception {
        System.err.println(processDefineMapper.selectCount(Wrappers.lambdaQuery()));
    }
}
