package com.mldong.flow.engine.service;

import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.entity.ProcessTask;
import com.mldong.flow.engine.model.TaskModel;

import java.util.List;

/**
 *
 * 流程任务服务
 * @author mldong
 * @date 2023/5/30
 */
public interface ProcessTaskService {
    /**
     * 保存流程任务
     * @param processTask
     */
    void saveProcessTask(ProcessTask processTask);

    /**
     * 更新流程任务
     * @param processTask
     */
    void updateProcessTask(ProcessTask processTask);

    /**
     * 根据流程实例ID获取正在进行的任务
     * @param processInstanceId
     * @return
     */
    List<ProcessTask> getDoingTaskList(Long processInstanceId);
    /**
     * 将流程任务修改为已完成
     * @param processTaskId
     * @param operator 任务处理人
     */
    void finishProcessTask(Long processTaskId, String operator);

    /**
     * 根据任务模型和流程执行对象创建任务
     * @param taskModel
     * @param execution
     * @return
     */
    List<ProcessTask> createTask(TaskModel taskModel, Execution execution);

    /**
     * 通过id获取流程任务
     * @param id
     * @return
     */
    ProcessTask getById(Long id);
    /**
     * 向指定的任务id添加参与者
     * @param processTaskId 任务id
     * @param actors 参与者
     */
    void addTaskActor(Long processTaskId, List<String> actors);

    /**
     * 向指定任务移除参与者
     * @param processTaskId 任务id
     * @param actors 参与者
     */
    void removeTaskActor(Long processTaskId, List<String> actors);
}
