package com.mldong.flow.engine.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mldong.flow.engine.AssignmentHandler;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.entity.ProcessTask;
import com.mldong.flow.engine.entity.ProcessTaskActor;
import com.mldong.flow.engine.enums.TaskStateEnum;
import com.mldong.flow.engine.mapper.ProcessTaskActorMapper;
import com.mldong.flow.engine.mapper.ProcessTaskMapper;
import com.mldong.flow.engine.model.TaskModel;
import com.mldong.flow.engine.service.ProcessTaskService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * 流程任务服务实现类
 * @author mldong
 * @date 2023/5/30
 */
@Service
@AllArgsConstructor
public class ProcessTaskServiceImpl implements ProcessTaskService {
    private final ProcessTaskMapper processTaskMapper;
    private final ProcessTaskActorMapper processTaskActorMapper;
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveProcessTask(ProcessTask processTask) {
        processTaskMapper.insert(processTask);
    }

    @Override
    public void updateProcessTask(ProcessTask processTask) {
        processTaskMapper.updateById(processTask);
    }

    @Override
    public List<ProcessTask> getDoingTaskList(Long processInstanceId) {
        List<ProcessTask> processTaskList = processTaskMapper.selectList(
                Wrappers.lambdaQuery(ProcessTask.class)
                        .eq(ProcessTask::getProcessInstanceId,processInstanceId)
                        .eq(ProcessTask::getTaskState, TaskStateEnum.DOING.getCode())
        );
        return processTaskList;
    }

    @Override
    public void finishProcessTask(Long processTaskId,String operator) {
        ProcessTask processTask = new ProcessTask();
        processTask.setId(processTaskId);
        processTask.setTaskState(TaskStateEnum.FINISHED.getCode());
        processTask.setUpdateTime(new Date());
        processTask.setUpdateUser(operator);
        processTask.setOperator(operator);
        processTaskMapper.updateById(processTask);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<ProcessTask> createTask(TaskModel taskModel, Execution execution) {
        List<ProcessTask> processTaskList = new ArrayList<>();
        Date now = new Date();
        ProcessTask processTask = new ProcessTask();
        processTask.setTaskName(taskModel.getName());
        processTask.setDisplayName(taskModel.getDisplayName());
        processTask.setTaskState(TaskStateEnum.DOING.getCode());
        processTask.setProcessInstanceId(execution.getProcessInstanceId());
        processTask.setVariable(JSONUtil.toJsonStr(execution.getArgs()));
        processTask.setCreateTime(now);
        processTask.setUpdateTime(now);
        processTaskMapper.insert(processTask);
        execution.setProcessTask(processTask);
        System.out.println("创建任务："+processTask.getTaskName()+","+processTask.getDisplayName());
        processTaskList.add(processTask);
        addTaskActor(processTask.getId(),getTaskActors(taskModel,execution));
        return processTaskList;
    }

    @Override
    public ProcessTask getById(Long id) {
        return processTaskMapper.selectById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addTaskActor(Long processTaskId, List<String> actors) {
        if(CollectionUtil.isEmpty(actors)) return;
        actors.forEach(actor->{
            ProcessTaskActor processTaskActor = new ProcessTaskActor();
            processTaskActor.setProcessTaskId(processTaskId);
            processTaskActor.setActorId(actor);
            processTaskActor.setCreateTime(new Date());
            System.out.println(StrUtil.format("给任务：{}，添加参与者：{}",processTaskId,actor));
            processTaskActorMapper.insert(processTaskActor);
        });
    }

    @Override
    public void removeTaskActor(Long processTaskId, List<String> actors) {
        processTaskActorMapper.delete(
                Wrappers.lambdaQuery(ProcessTaskActor.class)
                .eq(ProcessTaskActor::getProcessTaskId,processTaskId)
                .in(ProcessTaskActor::getActorId,actors)
        );
    }

    /**
     * 根据Task模型的assignee、assignmentHandler属性以及运行时数据，确定参与者
     * @param model 任务模型
     * @param execution 流程执行对象
     * @return
     */
    private List<String> getTaskActors(TaskModel model, Execution execution) {
        List<String> res = new ArrayList<>();
        Dict args = execution.getArgs();
        String assignee = model.getAssignee();
        // 参与者属性不为空
        if(StrUtil.isNotEmpty(assignee)) {
            // 多个使用英文逗号分割
            String [] assigneeArr = assignee.split(",");
            for (int i = 0; i < assigneeArr.length; i++) {
                // 如果args中存在assigneeArr[i]为key的数据==>参与者设置方式中的动态传递
                // 如果args中不存在assigneeArr[i]为key的数据==>参与者设置方式中的静态配置
                res.add(args.get(assigneeArr[i],assigneeArr[i]));
            }
        } else {
            String assignmentHandler = model.getAssignmentHandler();
            if(StrUtil.isNotEmpty(assignmentHandler)) {
                AssignmentHandler assignmentHandlerObj = ReflectUtil.newInstance(assignmentHandler);
                res.addAll(assignmentHandlerObj.assign(model,execution));
            }
        }
        return res;
    }
}
