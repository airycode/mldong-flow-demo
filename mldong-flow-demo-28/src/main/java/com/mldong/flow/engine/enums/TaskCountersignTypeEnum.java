package com.mldong.flow.engine.enums;

import cn.hutool.core.convert.Convert;

import java.util.Arrays;

/**
* @author mldong
* @date 2023/11/24
*/
public enum TaskCountersignTypeEnum {
    PARALLEL(0, "并行会签"),
    SEQUENTIAL(1, "串行会签"),
    ;
    private final Integer code;

    private final String message;
    public static TaskCountersignTypeEnum codeOf(Object code) {
        return Arrays.stream(TaskCountersignTypeEnum.class.getEnumConstants()).filter(e ->
                e.getCode().equals(Convert.toInt(code))
                        || e.name().equalsIgnoreCase(Convert.toStr(code))
                        || e.getMessage().equalsIgnoreCase(Convert.toStr(code))
        ).findAny().orElse(TaskCountersignTypeEnum.PARALLEL);
    }

    TaskCountersignTypeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
