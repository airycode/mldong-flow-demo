package com.mldong.flow.engine.enums;
/**
 *
 * 流程常量
 * @author mldong
 * @date 2023/6/9
 */
public interface FlowConst {
    // 业务流程号
    String BUSINESS_NO = "BUSINESS_NO";
    // 超级管理员ID
    String ADMIN_ID = "flow.admin";
    // 自动执行ID
    String AUTO_ID = "flow.auto";
    // 会签变量前辍
    String COUNTERSIGN_VARIABLE_PREFIX = "csv_";
    // 活跃的会签操作人数
    String NR_OF_ACTIVATE_INSTANCES = "nrOfActivateInstances";
    // 循环计数器，办理人在列表中的索引
    String LOOP_COUNTER = "loopCounter";
    // 会签总实例数
    String NR_OF_INSTANCES = "nrOfInstances";
    // 会签已完成实例数
    String NR_OF_COMPLETED_INSTANCES = "nrOfCompletedInstances";
    // 会签操作人列表
    String COUNTERSIGN_OPERATOR_LIST = "operatorList";
    // 会签类型 PARALLEL表示并行会签，SEQUENTIAL表示串行会签
    String COUNTERSIGN_TYPE = "countersignType";
    // 会签不同意标识
    String COUNTERSIGN_DISAGREE_FLAG = "countersignDisagreeFlag";
}
