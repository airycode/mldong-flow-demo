package com.mldong.interceptor;

import com.mldong.flow.engine.FlowInterceptor;
import com.mldong.flow.engine.core.Execution;
import org.springframework.stereotype.Component;

/**
 * @author mldong
 * @date 2023/11/6
 */
@Component
public class GlobalSendMessageInterceptor implements FlowInterceptor {
    @Override
    public void intercept(Execution execution) {
        execution.getProcessTaskList().forEach(processTask -> {
            System.out.println("全局拦截器："+processTask.getTaskName()+","+processTask.getDisplayName());
        });
    }
}
