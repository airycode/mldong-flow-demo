package com.mldong.flow;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.lang.Dict;
import com.mldong.flow.engine.FlowEngine;
import com.mldong.flow.engine.MockExecuteTask;
import com.mldong.flow.engine.cfg.Configuration;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.core.FlowEngineImpl;
import com.mldong.flow.engine.entity.ProcessInstance;
import com.mldong.flow.engine.entity.ProcessTask;
import com.mldong.flow.engine.enums.FlowConst;
import com.mldong.flow.engine.impl.SpringContext;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 * 流程引擎单元测试类
 * @author mldong
 * @date 2023/5/30
 */
public class FlowEngineTest extends BaseUnit{
    @Resource
    private SpringContext springContext;
    @Test
    public void executeLeave_28_1() {
        // 1.配置引擎
        FlowEngine engine = new FlowEngineImpl().configure(new Configuration(springContext));
        // 2. 部署一条流程
        Long processDefineId = engine.processDefineService().deploy(IoUtil.readUtf8(this.getClass().getResourceAsStream("/leave_23.json")));
        // 3. 启动一条流程实例
        Dict args = Dict.create();
        args.put("approve.operator","1");
        args.put("approveDept.operator","2");
        ProcessInstance processInstance = engine.startProcessInstanceById(processDefineId,"1",args);
        List<ProcessTask> processTaskList = engine.processTaskService().getDoingTaskList(processInstance.getId(),new String[]{});
        // 4. 手动增加代理人
        engine.processTaskService().addTaskActor(processTaskList.get(0).getId(), CollectionUtil.newArrayList("s_2"));
        // 5. 构建执行对象
        Execution execution = new Execution();
        execution.setArgs(Dict.create());
        execution.setEngine(engine);
        execution.setProcessInstanceId(processInstance.getId());
        execution.setProcessInstance(processInstance);
        // 6. 使用代理人去执行
        execution.setOperator("s_2");
        engine.executeProcessTask(processTaskList.get(0).getId(),"s_2",execution.getArgs());
    }
    @Test
    public void executeLeave_28_2() {
        // 1.配置引擎
        FlowEngine engine = new FlowEngineImpl().configure(new Configuration(springContext));
        // 2. 部署一条流程
        Long processDefineId = engine.processDefineService().deploy(IoUtil.readUtf8(this.getClass().getResourceAsStream("/leave_23.json")));
        // 3. 启动一条流程实例
        Dict args = Dict.create();
        args.put("approve.operator","1");
        args.put("approveDept.operator","2");
        ProcessInstance processInstance = engine.startProcessInstanceById(processDefineId,"1",args);
        // 4. 构建执行对象
        Execution execution = new Execution();
        execution.setArgs(Dict.create());
        execution.setEngine(engine);
        execution.setProcessInstanceId(processInstance.getId());
        execution.setProcessInstance(processInstance);
        // 5. 使用代理人去执行
        execution.setOperator("s_1");
        List<ProcessTask> processTaskList = engine.processTaskService().getDoingTaskList(processInstance.getId(),new String[]{});
        engine.executeProcessTask(processTaskList.get(0).getId(),"s_1",execution.getArgs());
    }
}
