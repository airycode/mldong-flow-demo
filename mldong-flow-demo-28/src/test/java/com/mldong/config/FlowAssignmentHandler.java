package com.mldong.config;

import cn.hutool.core.collection.CollectionUtil;
import com.mldong.flow.engine.AssignmentHandler;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.model.TaskModel;

import java.util.List;
/**
 *
 * 自定义参与者类实现
 * @author mldong
 * @date 2023/6/17
 */
public class FlowAssignmentHandler implements AssignmentHandler {
    @Override
    public List<String> assign(TaskModel model, Execution execution) {
        return CollectionUtil.newArrayList("3");
    }
}
