package com.mldong.flow;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.lang.Dict;
import com.mldong.flow.engine.cfg.Configuration;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.model.ProcessModel;
import com.mldong.flow.engine.parser.ModelParser;
import org.junit.Test;
/**
 *
 * 执行测试
 * @author mldong
 * @date 2023/5/1
 */
public class ExecuteTest {
    @Test
    public void executeLeave_01() {
        new Configuration();
        ProcessModel processModel = ModelParser.parse(IoUtil.readBytes(this.getClass().getResourceAsStream("/leave.json")));
        Execution execution = new Execution();
        execution.setArgs(Dict.create());
        processModel.getStart().execute(execution);
    }
    @Test
    public void executeLeave_02() {
        new Configuration();
        ProcessModel processModel = ModelParser.parse(IoUtil.readBytes(this.getClass().getResourceAsStream("/leave_02.json")));
        Execution execution = new Execution();
        execution.setArgs(Dict.create());
        processModel.getStart().execute(execution);
    }
    @Test
    public void executeLeave_02_1() {
        new Configuration();
        ProcessModel processModel = ModelParser.parse(IoUtil.readBytes(this.getClass().getResourceAsStream("/leave_02.json")));
        Execution execution = new Execution();
        execution.setArgs(Dict.create());
        execution.getArgs().put("f_day",1);
        processModel.getStart().execute(execution);
    }
    @Test
    public void executeLeave_02_2() {
        new Configuration();
        ProcessModel processModel = ModelParser.parse(IoUtil.readBytes(this.getClass().getResourceAsStream("/leave_02.json")));
        Execution execution = new Execution();
        execution.setArgs(Dict.create());
        execution.getArgs().put("f_day",3);
        processModel.getStart().execute(execution);
    }
    @Test
    public void executeLeave_03_1() {
        new Configuration();
        ProcessModel processModel = ModelParser.parse(IoUtil.readBytes(this.getClass().getResourceAsStream("/leave_03.json")));
        Execution execution = new Execution();
        execution.setArgs(Dict.create());
        execution.getArgs().put("f_day",1);
        processModel.getStart().execute(execution);
    }
    @Test
    public void executeLeave_03_2() {
        new Configuration();
        ProcessModel processModel = ModelParser.parse(IoUtil.readBytes(this.getClass().getResourceAsStream("/leave_03.json")));
        Execution execution = new Execution();
        execution.setArgs(Dict.create());
        execution.getArgs().put("f_day",3);
        processModel.getStart().execute(execution);
    }
    @Test
    public void executeLeave_04() {
        new Configuration();
        ProcessModel processModel = ModelParser.parse(IoUtil.readBytes(this.getClass().getResourceAsStream("/leave_04.json")));
        Execution execution = new Execution();
        execution.setArgs(Dict.create());
        processModel.getStart().execute(execution);
    }
}
