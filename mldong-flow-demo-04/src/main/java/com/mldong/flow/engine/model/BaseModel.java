package com.mldong.flow.engine.model;

import lombok.Data;
/**
 *
 * 模型基类
 * @author mldong
 * @date 2023/4/25
 */
@Data
public class BaseModel {
    private String name; // 唯一编码
    private String displayName; // 显示名称
}
