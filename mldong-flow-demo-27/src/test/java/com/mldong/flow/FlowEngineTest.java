package com.mldong.flow;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.lang.Dict;
import com.mldong.flow.engine.FlowEngine;
import com.mldong.flow.engine.MockExecuteTask;
import com.mldong.flow.engine.cfg.Configuration;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.core.FlowEngineImpl;
import com.mldong.flow.engine.entity.ProcessInstance;
import com.mldong.flow.engine.enums.FlowConst;
import com.mldong.flow.engine.impl.SpringContext;
import org.junit.Test;

import javax.annotation.Resource;

/**
 *
 * 流程引擎单元测试类
 * @author mldong
 * @date 2023/5/30
 */
public class FlowEngineTest extends BaseUnit{
    @Resource
    private SpringContext springContext;
    @Test
    public void executeLeave_27() {
        // 1.配置引擎
        FlowEngine engine = new FlowEngineImpl().configure(new Configuration(springContext));
        // 2. 部署一条流程
        Long processDefineId = engine.processDefineService().deploy(IoUtil.readUtf8(this.getClass().getResourceAsStream("/leave_23.json")));
        // 3. 启动一条流程实例
        Dict args = Dict.create();
        args.put("approve.operator","1");
        args.put("approveDept.operator","2");
        ProcessInstance processInstance = engine.startProcessInstanceById(processDefineId,"1",args);
        // 给用户1、2、3创建实例抄送
        engine.processInstanceService().createCCOrder(processInstance.getId(),"666","1","2","3");
        // 用户1修改抄送状态
        engine.processInstanceService().updateCCStatus(processInstance.getId(),"1");
    }
}
