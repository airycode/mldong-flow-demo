package com.mldong.flow.engine.cfg;


import com.mldong.flow.engine.Context;
import com.mldong.flow.engine.core.ServiceContext;
import com.mldong.flow.engine.impl.SimpleContext;
import com.mldong.flow.engine.parser.impl.*;
import com.mldong.flow.engine.service.impl.ProcessDefineServiceImpl;
import com.mldong.flow.engine.service.impl.ProcessInstanceServiceImpl;
import com.mldong.flow.engine.service.impl.ProcessTaskServiceImpl;
import org.apache.ibatis.session.SqlSessionFactory;
/**
 *
 * 流程引擎配置类
 * @author mldong
 * @date 2022/6/12
 */
public class Configuration {
    public Configuration() {
        this(new SimpleContext());
    }
    public Configuration(Context context) {
        ServiceContext.setContext(context);
        ServiceContext.put("decision", DecisionParser.class);
        ServiceContext.put("end", EndParser.class);
        ServiceContext.put("fork", ForkParser.class);
        ServiceContext.put("join", JoinParser.class);
        ServiceContext.put("start", StartParser.class);
        ServiceContext.put("task", TaskParser.class);
        SqlSessionFactory sqlSessionFactory = ServiceContext.find(SqlSessionFactory.class);
        ServiceContext.put("processDefineService",new ProcessDefineServiceImpl(sqlSessionFactory));
        ServiceContext.put("processInstanceService",new ProcessInstanceServiceImpl(sqlSessionFactory));
        ServiceContext.put("processTaskService",new ProcessTaskServiceImpl(sqlSessionFactory));

    }

}
