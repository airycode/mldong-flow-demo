package com.mldong.flow;

import cn.hutool.db.DbUtil;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.MybatisSqlSessionFactoryBuilder;
import com.mldong.flow.engine.mapper.ProcessDefineMapper;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;

import javax.sql.DataSource;

public class BaseTest {
    static SqlSessionFactory sqlSessionFactory = initSqlSessionFactory();
    //工厂方法
    public static SqlSessionFactory initSqlSessionFactory() {
        DataSource dataSource = DbUtil.getDs();
        TransactionFactory transactionFactory = new JdbcTransactionFactory();
        Environment environment = new Environment("FLOW", transactionFactory, dataSource);
        MybatisConfiguration configuration = new MybatisConfiguration(environment);
        //在这里添加Mapper
        configuration.addMapper(ProcessDefineMapper.class);
        return new MybatisSqlSessionFactoryBuilder().build(configuration);
    }
}
