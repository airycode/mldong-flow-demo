package com.mldong.flow.engine.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 流程任务和参与人关系
 * </p>
 *
 * @author mldong
 * @since 2023-05-28
 */
@Getter
@Setter
@TableName("wf_process_task_actor")
public class ProcessTaskActor implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 流程任务ID
     */
    private Long processTaskId;

    /**
     * 参与者ID
     */
    private String actorId;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 创建用户
     */
    @TableField(fill = FieldFill.INSERT)
    private String createUser;


}
