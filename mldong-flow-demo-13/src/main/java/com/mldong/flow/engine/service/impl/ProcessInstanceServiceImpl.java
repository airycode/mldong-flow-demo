package com.mldong.flow.engine.service.impl;

import com.mldong.flow.engine.entity.ProcessInstance;
import com.mldong.flow.engine.enums.ProcessInstanceStateEnum;
import com.mldong.flow.engine.mapper.ProcessInstanceMapper;
import com.mldong.flow.engine.service.ProcessInstanceService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * 流程实例服务实现类
 * @author mldong
 * @date 2023/5/30
 */
@Service
@AllArgsConstructor
public class ProcessInstanceServiceImpl implements ProcessInstanceService {
    private final ProcessInstanceMapper processInstanceMapper;
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void finishProcessInstance(Long processInstanceId) {
        ProcessInstance processInstance = new ProcessInstance();
        processInstance.setId(processInstanceId);
        processInstance.setState(ProcessInstanceStateEnum.FINISHED.getCode());
        processInstanceMapper.updateById(processInstance);
    }
}
