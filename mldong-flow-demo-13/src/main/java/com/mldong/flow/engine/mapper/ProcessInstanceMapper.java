package com.mldong.flow.engine.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mldong.flow.engine.entity.ProcessInstance;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 流程实例 Mapper 接口
 * </p>
 *
 * @author mldong
 * @since 2023-05-28
 */
@Mapper
public interface ProcessInstanceMapper extends BaseMapper<ProcessInstance> {

}
