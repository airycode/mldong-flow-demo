package com.mldong.interceptor;

import com.mldong.flow.engine.FlowInterceptor;
import com.mldong.flow.engine.core.Execution;

/**
 *
 * 部门领导审批节点前置拦截器
 * @author mldong
 * @date 2023/9/3
 */
public class ApproveDeptPostInterceptor implements FlowInterceptor {
    @Override
    public void intercept(Execution execution) {
        System.out.println("部门领导审批节点前置拦截器：ApproveDeptPostInterceptor");
    }
}
