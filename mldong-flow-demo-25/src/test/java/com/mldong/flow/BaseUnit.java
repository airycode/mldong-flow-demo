package com.mldong.flow;

import com.mldong.flow.engine.FlowApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlowApplication.class)
@WebAppConfiguration
public class BaseUnit {
}
