package com.mldong.flow;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.lang.Dict;
import com.mldong.flow.engine.MockExecuteTask;
import com.mldong.flow.engine.cfg.Configuration;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.model.ProcessModel;
import com.mldong.flow.engine.parser.ModelParser;
import org.junit.Test;
/**
 *
 * 执行测试
 * @author mldong
 * @date 2023/5/1
 */
public class ExecuteTest {
    @Test
    public void executeLeave_06() {
        new Configuration();
        // 将流程定义文件解析成流程模型
        ProcessModel processModel = ModelParser.parse(IoUtil.readBytes(this.getClass().getResourceAsStream("/leave_06.json")));
        // 构造执行参数
        Execution execution = new Execution();
        // 设置当前流程模型
        execution.setProcessModel(processModel);
        // 设置扩展属性
        execution.setArgs(Dict.create());
        // 拿到开始节点调用执行方法
        processModel.getStart().execute(execution);
        // 执行模拟执行任务方法
        new MockExecuteTask(execution).run();
    }
}
