package com.mldong.listener;

import com.mldong.flow.engine.event.ProcessEvent;
import com.mldong.flow.engine.event.ProcessEventListener;
import org.springframework.stereotype.Component;

/**
 * 默认流程监听器
 * @author mldong
 * @date 2023/12/5
 */
@Component
public class DefaultProcessEventListener implements ProcessEventListener {
    @Override
    public void onEvent(ProcessEvent event) {
        System.out.println(event.getEventType().getMessage()+" "+event.getSourceId());
    }
}
