package com.mldong.interceptor;

import com.mldong.flow.engine.FlowInterceptor;
import com.mldong.flow.engine.core.Execution;

/**
 *
 * 结束节点后置拦截器
 * @author mldong
 * @date 2023/9/3
 */
public class EndPostInterceptor implements FlowInterceptor {
    @Override
    public void intercept(Execution execution) {
        System.out.println("结束节点后置拦截器：EndPostInterceptor");
    }
}
