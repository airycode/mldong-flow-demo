package com.mldong.flow;

import cn.hutool.core.util.RandomUtil;
import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.DecisionHandler;
/**
 *
 * 决策类样例
 * @author mldong
 * @date 2023/5/1
 */
public class LeaveDecisionHandler implements DecisionHandler {
    @Override
    public String decide(Execution execution) {
        // 随机
        return RandomUtil.randomInt(1,10)%2==0?"approveBoss":"end";
    }
}
