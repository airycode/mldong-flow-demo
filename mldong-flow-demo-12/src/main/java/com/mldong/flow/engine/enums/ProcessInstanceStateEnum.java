package com.mldong.flow.engine.enums;

import java.util.Arrays;

/**
 *
 * 流程实例状态(10：进行中；20：已完成；30：已撤回；40：强行中止；50：挂起；99：已废弃)
 * @author mldong
 * @date 2023/5/28
 */
public enum ProcessInstanceStateEnum {
    DOING(10,"进行中"),
    FINISHED(20, "已完成"),
    WITHDRAW(30, "已撤回"),
    INTERRUPT(40, "强行中止"),
    PENDING(50,"挂起"),
    ABANDON(99,"已废弃")
    ;
    private final Integer code;

    private final String message;


    ProcessInstanceStateEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
    /**
     * code转成枚举
     * @param code
     * @return
     */
    public static ProcessInstanceStateEnum codeOf(Integer code) {
        return Arrays.stream(ProcessInstanceStateEnum.class.getEnumConstants()).filter(e -> e.getCode().equals(code)).findAny().orElse(DOING);
    }
}
