package com.mldong.flow.engine.service;

import com.mldong.flow.engine.entity.ProcessTask;

import java.util.List;

/**
 *
 * 流程任务服务
 * @author mldong
 * @date 2023/5/30
 */
public interface ProcessTaskService {
    /**
     * 保存流程任务
     * @param processTask
     */
    void saveProcessTask(ProcessTask processTask);

    /**
     * 根据流程实例ID获取正在进行的任务
     * @param processInstanceId
     * @return
     */
    List<ProcessTask> getDoingTaskList(Long processInstanceId);
}
