package com.mldong.flow.engine.entity;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 流程任务
 * @author mldong
 * @date 2023/5/17
 */
@Data
public class ProcessTask implements Serializable {
    private Long id; // 主键
    private String taskName; // 任务名称
    private String displayName; // 任务显示名称
    private Integer taskState; // 任务状态
}
