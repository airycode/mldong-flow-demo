package com.mldong.flow.engine.enums;
/**
 *
 * 任务状态枚举
 * @author mldong
 * @date 2023/5/17
 */
public enum TaskStateEnum {
    DOING(10,"进行中"),
    FINISHED(20, "已完成"),
    ;
    private final Integer code;

    private final String message;


    TaskStateEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
