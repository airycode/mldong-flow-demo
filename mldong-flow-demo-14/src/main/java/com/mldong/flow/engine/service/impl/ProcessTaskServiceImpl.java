package com.mldong.flow.engine.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mldong.flow.engine.entity.ProcessTask;
import com.mldong.flow.engine.enums.TaskStateEnum;
import com.mldong.flow.engine.mapper.ProcessTaskMapper;
import com.mldong.flow.engine.service.ProcessTaskService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * 流程任务服务实现类
 * @author mldong
 * @date 2023/5/30
 */
@Service
@AllArgsConstructor
public class ProcessTaskServiceImpl implements ProcessTaskService {
    private final ProcessTaskMapper processTaskMapper;
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveProcessTask(ProcessTask processTask) {
        processTaskMapper.insert(processTask);
    }

    @Override
    public List<ProcessTask> getDoingTaskList(Long processInstanceId) {
        List<ProcessTask> processTaskList = processTaskMapper.selectList(
                Wrappers.lambdaQuery(ProcessTask.class)
                        .eq(ProcessTask::getProcessInstanceId,processInstanceId)
                        .eq(ProcessTask::getTaskState, TaskStateEnum.DOING.getCode())
        );
        return processTaskList;
    }
}
