package com.mldong.flow.engine.service.impl;

import com.mldong.flow.engine.entity.ProcessInstance;
import com.mldong.flow.engine.enums.ProcessInstanceStateEnum;
import com.mldong.flow.engine.mapper.ProcessInstanceMapper;
import com.mldong.flow.engine.service.ProcessInstanceService;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

/**
 *
 * 流程实例服务实现类
 * @author mldong
 * @date 2023/5/30
 */
public class ProcessInstanceServiceImpl implements ProcessInstanceService {
    private SqlSessionFactory sqlSessionFactory;
    public ProcessInstanceServiceImpl(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }
    @Override
    public void finishProcessInstance(Long processInstanceId) {
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        ProcessInstanceMapper processInstanceMapper = sqlSession.getMapper(ProcessInstanceMapper.class);
        ProcessInstance processInstance = new ProcessInstance();
        processInstance.setId(processInstanceId);
        processInstance.setState(ProcessInstanceStateEnum.FINISHED.getCode());
        processInstanceMapper.updateById(processInstance);
    }
}
