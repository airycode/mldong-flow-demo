package com.mldong.flow.engine.service;
/**
 *
 * 流程实例服务
 * @author mldong
 * @date 2023/5/30
 */
public interface ProcessInstanceService {
    /**
     * 将流程实例修改为已完成
     * @param processInstanceId
     */
    void finishProcessInstance(Long processInstanceId);
}
