package com.mldong.flow.engine.service;
/**
 *
 * 流程定义服务
 * @author mldong
 * @date 2023/5/30
 */
public interface ProcessDefineService {
    /**
     * 部署流程
     * @param json
     */
    Long deploy(String json);
}
