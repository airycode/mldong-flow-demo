package com.mldong.flow.engine.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * 流程模型
 * @author mldong
 * @date 2023/4/25
 */
@Data
public class ProcessModel extends BaseModel {
    private String type; // 流程定义分类
    private String instanceUrl; // 启动实例要填写的表单key
    private String expireTime; // 期待完成时间变量key
    private String instanceNoClass; // 实例编号生成器实现类
    // 流程定义的所有节点
    private List<NodeModel> nodes = new ArrayList<NodeModel>();
    // 流程定义的所有任务节点
    private List<TaskModel> tasks = new ArrayList<TaskModel>();

}
