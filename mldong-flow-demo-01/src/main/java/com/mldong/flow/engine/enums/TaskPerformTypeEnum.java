package com.mldong.flow.engine.enums;

import java.util.Arrays;
/**
 *
 * 任务参与类型
 * @author mldong
 * @date 2023/4/25
 */
public enum TaskPerformTypeEnum {
    /**
     * 普通参与：一个或多个人同时参与一个任务，所有人只要其中一人执行完成，就能驱动任务节点往下一步执行
     */
    NORMAL(0,"普通参与"),
    /**
     * 会签参与：给每个人都创建任务，满足一定条件时，才能驱动任务节点往下一步执行
     * 1. 所有人都执行完成
     * 2. 有一定比率执行完成时
     * 3. 某特殊人员执行完成时
     * ……等等
     */
    COUNTERSIGN(1, "会签参与"),
    ;
    private final Integer code;

    private final String message;


    TaskPerformTypeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    /**
     * code转成枚举
     * @param code
     * @return
     */
    public static TaskPerformTypeEnum codeOf(Integer code) {
        return Arrays.stream(TaskPerformTypeEnum.class.getEnumConstants()).filter(e -> e.getCode().equals(code)).findAny().orElse(NORMAL);
    }
}
