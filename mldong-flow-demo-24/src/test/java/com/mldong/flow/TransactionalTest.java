package com.mldong.flow;

import com.mldong.flow.engine.entity.ProcessTaskActor;
import com.mldong.flow.engine.mapper.ProcessTaskActorMapper;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
/**
 *
 * 事务测试
 * @author mldong
 * @date 2023/6/4
 */
public class TransactionalTest extends BaseUnit {
    @Resource
    private ProcessTaskActorMapper processTaskActorMapper;
    @Transactional(rollbackFor = Exception.class)
    @Test
    public void transTest() {
        ProcessTaskActor processTaskActor = new ProcessTaskActor();
        processTaskActor.setActorId("1");
        processTaskActor.setId(1L);
        processTaskActor.setProcessTaskId(1L);
        processTaskActor.setCreateTime(new Date());
        processTaskActor.setCreateUser("1");
        processTaskActorMapper.insert(processTaskActor);
        int a= 2/0;
    }
    @Test
    public void noTransTest() {
        ProcessTaskActor processTaskActor = new ProcessTaskActor();
        processTaskActor.setActorId("2");
        processTaskActor.setId(2L);
        processTaskActor.setProcessTaskId(2L);
        processTaskActor.setCreateTime(new Date());
        processTaskActor.setCreateUser("2");
        processTaskActorMapper.insert(processTaskActor);
        int a= 2/0;
    }
}
