package com.mldong.interceptor;

import com.mldong.flow.engine.FlowInterceptor;
import com.mldong.flow.engine.core.Execution;

/**
 *
 * 请假流程前置拦截器
 * @author mldong
 * @date 2023/9/10
 */
public class LeavePreInterceptor implements FlowInterceptor {
    @Override
    public void intercept(Execution execution) {
        System.out.println("请假流程前置拦截器：LeavePreInterceptor,当前节点名："+execution.getNodeModel().getName());
    }
}
