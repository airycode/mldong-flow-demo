package com.mldong.interceptor;

import com.mldong.flow.engine.FlowInterceptor;
import com.mldong.flow.engine.core.Execution;
/**
 *
 * 开始节点后置拦截器
 * @author mldong
 * @date 2023/9/3
 */
public class StartPostInterceptor implements FlowInterceptor {
    @Override
    public void intercept(Execution execution) {
        System.out.println("开始节点后置拦截器：StartPostInterceptor");
    }
}
