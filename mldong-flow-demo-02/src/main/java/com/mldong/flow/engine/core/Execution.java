package com.mldong.flow.engine.core;

import cn.hutool.core.lang.Dict;
import lombok.Data;
/**
 *
 * 执行对象参数
 * @author mldong
 * @date 2023/4/25
 */
@Data
public class Execution {
    private String processInstanceId; // 流程实例ID
    private String processTaskId; // 当前流程任务ID
    // 执行对象扩展参数
    private Dict args;
}