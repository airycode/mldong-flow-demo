package com.mldong.flow.engine.model;

import com.mldong.flow.engine.core.Execution;
import lombok.Data;
/**
 *
 * 决策模型
 * @author mldong
 * @date 2023/4/25
 */
@Data
public class DecisionModel extends NodeModel {
    private String expr; // 决策表达式
    private String handleClass; // 决策处理类
    @Override
    public void exec(Execution execution) {
        // 执行决策节点自定义执行逻辑
    }
}