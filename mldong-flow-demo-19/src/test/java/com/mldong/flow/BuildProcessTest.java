package com.mldong.flow;

import com.mldong.flow.engine.core.Execution;
import com.mldong.flow.engine.model.*;
import org.junit.Test;

public class BuildProcessTest {
    @Test
    public void buildProcessModel() {
        ProcessModel processModel = new ProcessModel();
        StartModel startModel = new StartModel();
        startModel.setName("start");
        startModel.setDisplayName("开始");
        processModel.getNodes().add(startModel);
        TransitionModel t1 = new TransitionModel();
        t1.setName("t1");
        t1.setTo("apply");
        t1.setSource(startModel);
        startModel.getOutputs().add(t1);

        TaskModel applyTaskModel = new TaskModel();
        t1.setTarget(applyTaskModel);
        applyTaskModel.setName("apply");
        applyTaskModel.setDisplayName("请假申请");
        processModel.getNodes().add(applyTaskModel);
        processModel.getTasks().add(applyTaskModel);
        TransitionModel t2 = new TransitionModel();
        t2.setName("t2");
        t2.setTo("deptApprove");
        t2.setSource(applyTaskModel);
        applyTaskModel.getOutputs().add(t2);

        TaskModel deptApproveTaskModel = new TaskModel();
        t2.setTarget(deptApproveTaskModel);
        deptApproveTaskModel.setName("deptApprove");
        deptApproveTaskModel.setDisplayName("部门领导审批");
        processModel.getNodes().add(deptApproveTaskModel);
        processModel.getTasks().add(deptApproveTaskModel);

        TransitionModel t3 = new TransitionModel();
        t3.setName("t3");
        t3.setTo("end");
        t3.setSource(deptApproveTaskModel);
        deptApproveTaskModel.getOutputs().add(t3);

        EndModel endModel = new EndModel();
        t3.setTarget(endModel);
        endModel.setName("end");
        endModel.setDisplayName("结束");
        processModel.getNodes().add(endModel);
        // 打印开始节点
        processModel.getStart().execute(new Execution());
    }
}