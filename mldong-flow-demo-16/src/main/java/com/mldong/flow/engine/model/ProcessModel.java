package com.mldong.flow.engine.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * 流程模型
 * @author mldong
 * @date 2023/4/25
 */
@Data
public class ProcessModel extends BaseModel {
    private String type; // 流程定义分类
    private String instanceUrl; // 启动实例要填写的表单key
    private String expireTime; // 期待完成时间变量key
    private String instanceNoClass; // 实例编号生成器实现类
    // 流程定义的所有节点
    private List<NodeModel> nodes = new ArrayList<NodeModel>();
    // 流程定义的所有任务节点
    private List<TaskModel> tasks = new ArrayList<TaskModel>();

    /**
     * 获取开始节点
     * @return
     */
    public StartModel getStart() {
        StartModel startModel = null;
        for (int i = 0; i < nodes.size(); i++) {
            NodeModel nodeModel = nodes.get(i);
            if(nodeModel instanceof StartModel) {
                startModel = (StartModel) nodeModel;
                break;
            }
        }
        return startModel;
    }
    /**
     * 获取process定义的指定节点名称的节点模型
     * @param nodeName 节点名称
     * @return
     */
    public NodeModel getNode(String nodeName) {
        for(NodeModel node : nodes) {
            if(node.getName().equals(nodeName)) {
                return node;
            }
        }
        return null;
    }
}
