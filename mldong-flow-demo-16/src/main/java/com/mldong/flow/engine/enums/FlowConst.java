package com.mldong.flow.engine.enums;
/**
 *
 * 流程常量
 * @author mldong
 * @date 2023/6/9
 */
public interface FlowConst {
    // 业务流程号
    public final static String BUSINESS_NO = "BUSINESS_NO";
}
